import React from 'react';
import { Button as RNButton, ButtonProps } from '@rneui/themed';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  button: {
    borderRadius: 5,
    marginBottom: 10,
  },
});

const Button: React.FC<ButtonProps> = props => <RNButton {...props} buttonStyle={styles.button} />;

export { Button };
