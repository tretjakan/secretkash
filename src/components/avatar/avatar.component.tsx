import React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Icon } from '@rneui/themed';
import { IAvatar } from '@shared/models';
import { Colors } from '@shared/colors';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
});

const Avatar: React.FC<IAvatar> = ({ iconSize = 25, label }) => {
  return (
    <View style={styles.container}>
      <Icon reverse name="person" size={iconSize} color={Colors.dark} />
      <Text>{label}</Text>
    </View>
  );
};

export { Avatar };
