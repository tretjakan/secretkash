import React from 'react';
import { View, Image, StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  logo: {
    width: 150,
    height: 100,
  },
});

const Logo = () => (
  <View>
    <Image source={require('../../assets/img/logo.png')} style={styles.logo} />
  </View>
);

export { Logo };
