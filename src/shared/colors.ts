enum Colors {
  main = '#B5525C',
  primary = '#00587A',
  secondary = '#105E62',
  palette = '#839AA8',
  component = '#D2FAFB',
  neutral = '#E8F9FD',
  dark = '#04293A',
}

export { Colors };
