export interface IJoinGame {
  isOpen: boolean;
  onClose: () => void;
}

export interface ICreateGame {
  isOpen: boolean;
  onClose: () => void;
}

export interface IAvatar {
  label: string;
  iconSize?: number;
}

export interface ISession {
  users: {
    id: string;
    type: 'pending' | 'ready' | 'eliminated';
    role?: string;
    username: string;
  }[];
  isActive: boolean;
  configuration: {
    amountOfPlayers: number;
  };
  _id: string;
  status: 'pending' | 'closed' | 'active';
  rules: any[];
  name: string;
}

export interface IModal {
  isOpen: boolean;
  onClose: () => void;
}

export type VoteType = 'councilor' | 'elimination';

export type IPlayer = ISession['users'][0];
