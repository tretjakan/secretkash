export interface ILogin {
  username: string;
  password: string;
}

export interface IUser {
  _id: string;
  username: string;
}
