export enum LoaderNames {
  sessionsCreate = 'sessionsCreate',
  sessionsJoin = 'sessionsJoin',
  sessionsLeave = 'sessionsLeave',
  sessionsAcceptRole = 'sessionsAcceptRole',
}

export enum RuleTypes {
  liberal = 'liberal',
  nazi = 'nazi',
}

export const MAX_UNVOTED_ROUNDS = 3;
