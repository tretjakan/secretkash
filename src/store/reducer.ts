import { combineReducers } from 'redux';

import { auth, AuthSaga, AuthState } from './auth';
import { loaders, LoaderState } from './loaders';
import { sessions, SessionSaga, SessionState } from './sessions';

const rootReducer = combineReducers<{
  auth: AuthState;
  sessions: SessionState;
  loaders: LoaderState;
}>({
  auth,
  sessions,
  loaders,
});

export type AppState = ReturnType<typeof rootReducer>;

const sagas = [AuthSaga.watch, SessionSaga.watch];

export { rootReducer, sagas };
