import { call, put, takeLatest } from 'redux-saga/effects';
import AsyncStorage from '@react-native-async-storage/async-storage';

import { authService } from '@services';

import { APP_INIT, login, LOGIN_INIT } from './actions';

const AuthSaga = {
  *login({ payload }: ReturnType<typeof login.init>) {
    try {
      const {
        data: { data },
      } = yield call(authService.login, payload);

      yield call(AsyncStorage.setItem, 'token', data.token);
      yield call(AsyncStorage.setItem, 'user', JSON.stringify(data.user));
      yield put(login.success(data));
    } catch (error) {
      console.error(error);
    }
  },

  *init() {
    try {
      const token: string = yield call(AsyncStorage.getItem, 'token');
      const user: string = yield call(AsyncStorage.getItem, 'user');

      if (token) {
        yield put(login.success({ token, user: JSON.parse(user) }));
      }
    } catch (error) {
      console.error(error);
    }
  },

  *watch() {
    yield takeLatest(LOGIN_INIT, AuthSaga.login);
    yield takeLatest(APP_INIT, AuthSaga.init);
  },
};

export { AuthSaga };
