import { createSelector } from 'reselect';

import { AppState } from '../reducer';

const getState = (state: AppState) => state.auth;

export const selectIsSignedIn = createSelector(getState, state => state.token);
export const selectUser = createSelector(getState, state => state.user);
