import { ILogin, IUser } from '@shared/models';
import { APP_INIT, LOGIN_INIT, LOGIN_SUCCESS, LOGOUT } from './actions';

export interface LoginInit {
  type: typeof LOGIN_INIT;
  payload: ILogin;
}

export interface LoginSuccess {
  type: typeof LOGIN_SUCCESS;
  payload: { token: string; user: IUser };
}

export interface InitApp {
  type: typeof APP_INIT;
}

export interface Logout {
  type: typeof LOGOUT;
}

export interface AuthState {
  token?: string;
  user: IUser;
}

export type AuthActions = LoginInit | LoginSuccess | Logout;
