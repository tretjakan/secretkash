import { LoginInit, LoginSuccess, Logout, InitApp } from './props';

export const APP_INIT = '[APP]_INIT';
export const LOGIN_INIT = '[AUTH]_LOGIN_INIT';
export const LOGIN_SUCCESS = '[AUTH]_LOGIN_SUCCESS';
export const LOGOUT = '[AUTH]_LOGOUT';

export const login = {
  init: (payload: LoginInit['payload']): LoginInit => ({
    type: LOGIN_INIT,
    payload,
  }),
  success: (token: LoginSuccess['payload']): LoginSuccess => ({
    type: LOGIN_SUCCESS,
    payload: token,
  }),
};

export const logout = (): Logout => ({
  type: LOGOUT,
});

export const initApp = (): InitApp => ({
  type: APP_INIT,
});
