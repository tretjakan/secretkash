import { IUser } from '@shared/models';
import { withProduce } from '@store/resolve';

import { LOGIN_SUCCESS, LOGOUT } from './actions';
import { AuthActions, AuthState } from './props';

const initialState: AuthState = {
  token: undefined,
  user: {} as IUser,
};

const reducer = (state: AuthState, action: AuthActions) => {
  switch (action.type) {
    case LOGIN_SUCCESS: {
      state.token = action.payload.token;
      state.user = action.payload.user;
      break;
    }
    case LOGOUT: {
      state.token = undefined;
      state.user = {} as IUser;
      break;
    }
    default:
      break;
  }
};

const auth = withProduce(initialState, reducer);
export { auth };
