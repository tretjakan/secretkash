import { createStore, applyMiddleware, Middleware } from 'redux';
import createSagaMiddleware, { SagaMiddleware } from 'redux-saga';
import { composeWithDevTools } from '@redux-devtools/extension';

import { rootReducer, sagas } from './reducer';

const sagaMiddleware = createSagaMiddleware() as SagaMiddleware<any> & Middleware;
const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(sagaMiddleware)));
sagas.map(sagaMiddleware.run);

export { store };
