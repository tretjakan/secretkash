import { withProduce } from '@store/resolve';

import { STOP_LOADING, START_LOADING } from './actions';
import { LoaderState, LoaderActions } from './props';

const initialState: LoaderState = {
  names: [],
};

const reducer = (state: LoaderState, action: LoaderActions) => {
  switch (action.type) {
    case START_LOADING: {
      state.names = state.names.concat([action.payload]);
      break;
    }
    case STOP_LOADING: {
      state.names = state.names.filter(name => name !== action.payload);
      break;
    }
    default:
      break;
  }
};

const loaders = withProduce(initialState, reducer);
export { loaders };
