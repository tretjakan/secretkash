import { createSelector } from 'reselect';

import { AppState } from '../reducer';

const getState = (state: AppState) => state.loaders;

export const selectIsLoading = (name: string) =>
  createSelector(getState, state => state.names.some(item => name === item));
