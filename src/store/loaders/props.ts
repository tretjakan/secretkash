import { START_LOADING, STOP_LOADING } from './actions';

export interface StartLoading {
  type: typeof START_LOADING;
  payload: string;
}

export interface StopLoading {
  type: typeof STOP_LOADING;
  payload: string;
}

export interface LoaderState {
  names: string[];
}

export type LoaderActions = StartLoading | StopLoading;
