import { StartLoading, StopLoading } from './props';

export const START_LOADING = '[LOADER]_START';
export const STOP_LOADING = '[LOADER]_STOP';

export const loading = {
  start: (name: StartLoading['payload']): StartLoading => ({
    type: START_LOADING,
    payload: name,
  }),
  stop: (name: StartLoading['payload']): StopLoading => ({
    type: STOP_LOADING,
    payload: name,
  }),
};
