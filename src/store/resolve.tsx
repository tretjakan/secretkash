import React, { ComponentType } from 'react';
import produce from 'immer';
import { Provider } from 'react-redux';

import { store } from './index';

const withProduce = (initialState: any, reducers: any) =>
  produce((draftState = initialState, action) => {
    reducers(draftState, action);
    return draftState;
  });

const withProvider = (Component: ComponentType) => (props: any) =>
  (
    <Provider store={store}>
      <Component {...props} />
    </Provider>
  );

export { withProduce, withProvider };
