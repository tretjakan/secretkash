import { ISession } from '@shared/models';
import {
  CHECK_SESSION,
  CONNECT_SESSION,
  CREATE_SESSION_INIT,
  SET_SESSION,
  JOIN_SESSION_INIT,
  LEAVE_SESSION_INIT,
  LEAVE_SESSION_SUCCESS,
  SET_PRESIDENT,
  SET_COUNCILOR,
  DISCONNECT_SESSION,
  ACCEPT_ROLE,
  PUBLISH_MESSAGE,
  VOTE_COUNCILOR,
  SET_PROPOSED_COUNCILOR,
  SET_ROUND,
  SET_UNVOTED_ROUND,
} from './actions';

export interface SessionInit {
  type: typeof CREATE_SESSION_INIT;
  payload: { amountOfPlayers: number; callback: () => void };
}

export interface SessionSuccess {
  type: typeof SET_SESSION;
  payload: ISession;
}

export interface SetPresident {
  type: typeof SET_PRESIDENT;
  payload: string;
}

export interface SetCouncilor {
  type: typeof SET_COUNCILOR;
  payload: string;
}

export interface LeaveSessionInit {
  type: typeof LEAVE_SESSION_INIT;
  payload?: () => void;
}

export interface JoinSessionInit {
  type: typeof JOIN_SESSION_INIT;
  payload: {
    callback: () => void;
    name: string;
  };
}

export interface JoinSessionSuccess {
  type: typeof LEAVE_SESSION_INIT;
  payload: () => void;
}

export interface LeaveSessionSuccess {
  type: typeof LEAVE_SESSION_SUCCESS;
}

export interface CheckSession {
  type: typeof CHECK_SESSION;
}

export interface ConnectSession {
  type: typeof CONNECT_SESSION;
}

export interface DisconnectSession {
  type: typeof DISCONNECT_SESSION;
}

export interface PublishMessage {
  type: typeof PUBLISH_MESSAGE;
  payload: {
    event: string;
    data: any;
  };
}

export interface AcceptRole {
  type: typeof ACCEPT_ROLE;
}

export interface VoteCouncilor {
  type: typeof VOTE_COUNCILOR;
  payload: { userId: string; answer: boolean };
}

export interface SetProposedCouncilor {
  type: typeof SET_PROPOSED_COUNCILOR;
  payload: string;
}

export interface SetRound {
  type: typeof SET_ROUND;
  payload: number;
}

export interface SetUnvotedRound {
  type: typeof SET_UNVOTED_ROUND;
  payload?: number;
}

export interface SessionState {
  session: ISession;
  president: string;
  councilor: string;
  proposedCouncilor: string;
  councilorVotes: {
    users: string[];
    yes: number;
    no: number;
  };
  round: number;
  unvotedRounds: number;
}

export type SessionActions =
  | SessionInit
  | SessionSuccess
  | LeaveSessionSuccess
  | SetCouncilor
  | SetPresident
  | AcceptRole
  | VoteCouncilor
  | SetProposedCouncilor
  | SetRound
  | SetUnvotedRound;
