import { ISession } from '@shared/models';
import { withProduce } from '@store/resolve';

import {
  SET_SESSION,
  LEAVE_SESSION_SUCCESS,
  SET_PRESIDENT,
  SET_COUNCILOR,
  VOTE_COUNCILOR,
  SET_PROPOSED_COUNCILOR,
  SET_ROUND,
  SET_UNVOTED_ROUND,
} from './actions';
import { SessionState, SessionActions } from './props';

const initialState: SessionState = {
  session: {} as ISession,
  president: '',
  councilor: '',
  proposedCouncilor: '',
  councilorVotes: { yes: 0, no: 0, users: [] },
  round: 0,
  unvotedRounds: 0,
};

const reducer = (state: SessionState, action: SessionActions) => {
  switch (action.type) {
    case SET_SESSION: {
      state.session = action.payload;
      break;
    }
    case LEAVE_SESSION_SUCCESS: {
      state.session = {} as ISession;
      state.councilor = '';
      state.president = '';
      break;
    }
    case SET_PRESIDENT: {
      state.president = action.payload;
      break;
    }
    case VOTE_COUNCILOR: {
      if (state.councilorVotes.users.includes(action.payload.userId)) {
        break;
      }
      state.councilorVotes[action.payload.answer ? 'yes' : 'no'] += 1;
      state.councilorVotes.users = state.councilorVotes.users.concat([action.payload.userId]);
      break;
    }
    case SET_COUNCILOR: {
      state.councilor = action.payload;
      break;
    }
    case SET_PROPOSED_COUNCILOR: {
      state.proposedCouncilor = action.payload;
      if (action.payload === '') {
        state.councilorVotes = {
          yes: 0,
          no: 0,
          users: [],
        };
      }
      break;
    }
    case SET_ROUND: {
      state.round = action.payload;
      break;
    }
    case SET_UNVOTED_ROUND: {
      if (action.payload != null) {
        state.unvotedRounds = action.payload;
      } else {
        state.unvotedRounds += 1;
      }
      break;
    }
    // case VOTE: {
    //   if (!state.votes[action.payload]) {
    //     state.votes[action.payload] = 1;
    //   } else {
    //     state.votes[action.payload] += 1;
    //   }
    //   break;
    // }
    default:
      break;
  }
};

const sessions = withProduce(initialState, reducer);
export { sessions };
