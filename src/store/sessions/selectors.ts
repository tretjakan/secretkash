import { createSelector } from 'reselect';

import { AppState } from '../reducer';

const getState = (state: AppState) => state.sessions;

export const selectCurrentSession = createSelector(getState, state => state.session);
export const selectPresident = createSelector(getState, state => state.president);
export const selectCouncilor = createSelector(getState, state => state.councilor);
export const selectProposedCouncilor = createSelector(getState, state => state.proposedCouncilor);
export const selectCouncilorVotes = createSelector(getState, state => state.councilorVotes);
export const selectRound = createSelector(getState, state => state.round);
export const selectUnvotedRounds = createSelector(getState, state => state.unvotedRounds);
export const selectPlayer = createSelector(
  (state: AppState) => state,
  state => state.sessions.session.users.find(user => user.id === state.auth.user._id),
);
export const selectIfProposedCouncilorVotesCompleted = createSelector(
  getState,
  state => state.councilorVotes.users.length === state.session.configuration.amountOfPlayers,
);

export const selectIfPlayerChoseProposedCouncilor = createSelector(
  (state: AppState) => state,
  state => state.sessions.councilorVotes.users.some(user => user === state.auth.user._id),
);
