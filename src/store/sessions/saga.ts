import { call, put, select, take, takeEvery, takeLatest, fork } from 'redux-saga/effects';
import { EventChannel, eventChannel } from 'redux-saga';
import AsyncStorage from '@react-native-async-storage/async-storage';
import io, { Socket } from 'socket.io-client';

import { sessionService } from '@services';
import { loading } from '@store/loaders';
import { LoaderNames, MAX_UNVOTED_ROUNDS } from '@shared/enums';

import {
  CREATE_SESSION_INIT,
  createSession,
  leaveSession,
  CHECK_SESSION,
  LEAVE_SESSION_INIT,
  CONNECT_SESSION,
  setSession,
  JOIN_SESSION_INIT,
  joinSession,
  setPresident,
  DISCONNECT_SESSION,
  ACCEPT_ROLE,
  setCouncilor,
  SET_PRESIDENT,
  SET_COUNCILOR,
  publishMessage,
  voteCouncilor,
  PUBLISH_MESSAGE,
  setProposedCouncilor,
  setRound,
  setUnvotedRound,
} from './actions';
import { ISession, IUser } from '@shared/models';
import { selectUser } from '@store/auth';

import {
  selectCouncilorVotes,
  selectCurrentSession,
  selectIfProposedCouncilorVotesCompleted,
  selectPresident,
  selectProposedCouncilor,
  selectUnvotedRounds,
} from './selectors';
import { shuffleArray } from '@shared/helpers';
import { getStoreData, removeStoreData } from './helpers';
import { SessionState } from './props';

const SessionSaga = {
  *create({ payload }: ReturnType<typeof createSession.init>) {
    try {
      yield put(loading.start(LoaderNames.sessionsCreate));
      const {
        data: { data: session },
      } = yield call(sessionService.create, payload.amountOfPlayers);

      yield call(AsyncStorage.setItem, 'session', JSON.stringify(session));
      yield put(setSession(session));
      payload.callback();
    } catch (error) {
      console.error(error);
    } finally {
      yield put(loading.stop(LoaderNames.sessionsCreate));
    }
  },

  *join({ payload }: ReturnType<typeof joinSession.init>) {
    try {
      yield put(loading.start(LoaderNames.sessionsJoin));
      const user: IUser = yield select(selectUser);
      const {
        data: { data: session },
      } = yield call(sessionService.join, user._id, payload.name);

      yield call(AsyncStorage.setItem, 'session', JSON.stringify(session));
      yield put(setSession(session));
      payload.callback();
    } catch (error) {
      console.error(error);
    } finally {
      yield put(loading.stop(LoaderNames.sessionsJoin));
    }
  },

  *sessionCheck() {
    try {
      const { storedSession, president } = yield call(getStoreData);

      if (!storedSession) {
        return;
      }

      try {
        const {
          data: { data: session },
        } = yield call(sessionService.get, storedSession._id);

        if (session.status === 'closed') {
          yield call(removeStoreData);
          return;
        }
        yield put(setSession(session));
        yield put(setPresident(president));
      } catch (error) {
        yield call(removeStoreData);
      }
    } catch (error) {
      console.error(error);
    }
  },

  *leaveSession({ payload: callback }: ReturnType<typeof leaveSession.init>) {
    try {
      yield put(loading.start(LoaderNames.sessionsLeave));
      const user: IUser = yield select(selectUser);
      const session: ISession = yield select(selectCurrentSession);

      yield call(sessionService.leave, user._id, session.name);
      yield call(removeStoreData);

      yield put(leaveSession.success());
      if (typeof callback === 'function') {
        callback();
      }
    } catch (error) {
      console.error(error);
    } finally {
      yield put(loading.stop(LoaderNames.sessionsLeave));
    }
  },

  *acceptRole() {
    try {
      yield put(loading.start(LoaderNames.sessionsAcceptRole));
      const user: IUser = yield select(selectUser);
      const session: ISession = yield select(selectCurrentSession);

      yield call(sessionService.updatePlayer, {
        userId: user._id,
        sessionId: session.name,
        type: 'ready',
      });
    } catch (error) {
      console.error(error);
    } finally {
      yield put(loading.stop(LoaderNames.sessionsAcceptRole));
    }
  },

  *setRole({ payload, type }: ReturnType<typeof setPresident | typeof setCouncilor>) {
    if (type === SET_PRESIDENT) {
      AsyncStorage.setItem('president', payload);
    } else {
      AsyncStorage.setItem('councilor', payload);
    }
  },

  /**
   * Socket event
   * TODO: Move to separate file
   */
  *usersUpdate(session: ISession, prevSession: ISession) {
    yield put(setSession(session));
    yield call(AsyncStorage.setItem, 'session', JSON.stringify(session));
    if (prevSession.status !== 'active' && session.status === 'active') {
      const [president] = shuffleArray(session.users.map((user: { id: string }) => user.id));
      console.log('president', president);
      yield put(setPresident(session.users[0].id));
    }
  },

  *voteCouncilor(value: { userId: string; answer: boolean }) {
    const proposedCounciler: string = yield select(selectProposedCouncilor);

    yield put(voteCouncilor({ userId: value.userId, answer: value.answer }));

    const isVotingCompleted: boolean = yield select(selectIfProposedCouncilorVotesCompleted);

    if (!isVotingCompleted) {
      return;
    }

    const councilorVotes: SessionState['councilorVotes'] = yield select(selectCouncilorVotes);

    if (councilorVotes.yes > councilorVotes.no) {
      yield put(setCouncilor(proposedCounciler));
      yield put(setProposedCouncilor(''));
      yield put(setRound(1));
    } else {
      const unvotedRounds: number = yield select(selectUnvotedRounds);
      yield put(setProposedCouncilor(''));
      if (unvotedRounds + 1 === MAX_UNVOTED_ROUNDS) {
        const session: ISession = yield select(selectCurrentSession);
        const president: string = yield select(selectPresident);
        yield put(setUnvotedRound(0));

        const currentPresidentIndex = session.users.findIndex(user => user.id === president);
        if (currentPresidentIndex === session.users.length - 1) {
          yield put(setPresident(session.users[0].id));
        } else {
          yield put(setPresident(session.users[currentPresidentIndex + 1].id));
        }
      } else {
        yield put(setUnvotedRound());
      }
    }
  },

  socketInstance: {} as Socket,

  *connect() {
    const session: ISession = yield select(selectCurrentSession);
    const channel: EventChannel<any> = yield call(SessionSaga.createConnection, session._id);

    yield takeEvery(channel, function* (data: { value: any; event: string }) {
      switch (data.event) {
        case 'users': {
          yield fork(SessionSaga.usersUpdate, data.value as ISession, session);
          break;
        }
        case 'vote-councilor': {
          yield fork(SessionSaga.voteCouncilor, data.value);
          break;
        }
        case 'select-councilor': {
          yield put(setProposedCouncilor(data.value));
          break;
        }
        default: {
          console.info(`No handler for event ${data.event}`);
        }
      }
    });

    yield take(DISCONNECT_SESSION);
    channel.close();
  },

  *createConnection(sessionId: string) {
    return eventChannel(emit => {
      console.log('connection...');
      const socket = io('ws://localhost:8080', { query: { sessionId } });
      SessionSaga.socketInstance = socket;

      socket.on('message', (data: any) => {
        console.log('message from eventChannel', data);
        emit(data);
      });

      return () => {
        console.log('disconnect');
        emit(leaveSession.init());
        socket.disconnect();
      };
    });
  },

  *publish({ payload }: ReturnType<typeof publishMessage>) {
    try {
      const { socketInstance } = SessionSaga;
      if (!Object.keys(socketInstance).length || !socketInstance.connected) {
        console.error('Socket is not connected');
        return;
      }

      socketInstance.emit(payload.event, payload.data);
    } catch (error) {
      console.error('Publish socket error', error);
    }
  },

  *watch() {
    yield takeLatest(CREATE_SESSION_INIT, SessionSaga.create);
    yield takeLatest(CHECK_SESSION, SessionSaga.sessionCheck);
    yield takeLatest(LEAVE_SESSION_INIT, SessionSaga.leaveSession);
    yield takeLatest(CONNECT_SESSION, SessionSaga.connect);
    yield takeLatest(JOIN_SESSION_INIT, SessionSaga.join);
    yield takeLatest(ACCEPT_ROLE, SessionSaga.acceptRole);
    yield takeLatest(SET_PRESIDENT, SessionSaga.setRole);
    yield takeLatest(SET_COUNCILOR, SessionSaga.setRole);
    yield takeLatest(PUBLISH_MESSAGE, SessionSaga.publish);
  },
};

export { SessionSaga };
