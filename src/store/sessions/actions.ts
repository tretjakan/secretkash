import {
  SessionInit,
  SessionSuccess,
  LeaveSessionInit,
  LeaveSessionSuccess,
  CheckSession,
  ConnectSession,
  JoinSessionInit,
  SetPresident,
  SetCouncilor,
  DisconnectSession,
  AcceptRole,
  PublishMessage,
  VoteCouncilor,
  SetProposedCouncilor,
  SetRound,
  SetUnvotedRound,
} from './props';

export const CREATE_SESSION_INIT = '[SESSION]_CREATE_INIT';
export const SET_SESSION = '[SESSION]_CREATE_SUCCESS';
export const LEAVE_SESSION_SUCCESS = '[SESSION]_LEAVE_SUCCESS';
export const LEAVE_SESSION_INIT = '[SESSION]_LEAVE_INIT';
export const CHECK_SESSION = '[SESSION]_CHECK_IF_SESSION_EXIST';
export const CONNECT_SESSION = '[SESSION]_CONNECT_SOCKET';
export const DISCONNECT_SESSION = '[SESSION]_DISCONNECT_SESSION';
export const JOIN_SESSION_INIT = '[SESSION]_JOIN_INIT';
export const JOIN_SESSION_SUCCESS = '[SESSION]_JOIN_SUCCESS';
export const SET_PRESIDENT = '[SESSION]_SET_PRESIDENT';
export const SET_COUNCILOR = '[SESSION]_SET_COUNCILOR';
export const SET_PROPOSED_COUNCILOR = '[SESSION]_SET_PROPOSED_COUNCILOR';
export const ACCEPT_ROLE = '[SESSION]_ACCEPT_ROLE';
export const PUBLISH_MESSAGE = '[SESSION]_PUBLISH_MESSAGE';
export const VOTE_COUNCILOR = '[SESSION]_VOTE_COUNCILOR';
export const SET_ROUND = '[SESSION]_SET_ROUND';
export const SET_UNVOTED_ROUND = '[SESSION]_SET_UNVOTED_ROUND';

export const createSession = {
  init: (payload: SessionInit['payload']): SessionInit => ({
    type: CREATE_SESSION_INIT,
    payload,
  }),
};

export const setSession = (payload: SessionSuccess['payload']): SessionSuccess => ({
  type: SET_SESSION,
  payload,
});

export const setPresident = (payload: SetPresident['payload']): SetPresident => ({
  type: SET_PRESIDENT,
  payload,
});

export const setCouncilor = (payload: SetCouncilor['payload']): SetCouncilor => ({
  type: SET_COUNCILOR,
  payload,
});

export const setProposedCouncilor = (
  payload: SetProposedCouncilor['payload'],
): SetProposedCouncilor => ({
  type: SET_PROPOSED_COUNCILOR,
  payload,
});

export const acceptRole = (): AcceptRole => ({
  type: ACCEPT_ROLE,
});

export const leaveSession = {
  init: (payload?: LeaveSessionInit['payload']): LeaveSessionInit => ({
    type: LEAVE_SESSION_INIT,
    payload,
  }),
  success: (): LeaveSessionSuccess => ({
    type: LEAVE_SESSION_SUCCESS,
  }),
};

export const joinSession = {
  init: (payload: JoinSessionInit['payload']): JoinSessionInit => ({
    type: JOIN_SESSION_INIT,
    payload,
  }),
};

export const checkSession = (): CheckSession => ({
  type: CHECK_SESSION,
});

export const connectSocket = (): ConnectSession => ({
  type: CONNECT_SESSION,
});

export const disconnectSocket = (): DisconnectSession => ({
  type: DISCONNECT_SESSION,
});

export const publishMessage = (payload: PublishMessage['payload']): PublishMessage => ({
  type: PUBLISH_MESSAGE,
  payload,
});

export const voteCouncilor = (payload: VoteCouncilor['payload']): VoteCouncilor => ({
  type: VOTE_COUNCILOR,
  payload,
});

export const setRound = (payload: SetRound['payload']): SetRound => ({
  type: SET_ROUND,
  payload,
});

export const setUnvotedRound = (payload?: SetUnvotedRound['payload']): SetUnvotedRound => ({
  type: SET_UNVOTED_ROUND,
  payload,
});
