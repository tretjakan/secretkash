import AsyncStorage from '@react-native-async-storage/async-storage';

const removeStoreData = async () =>
  Promise.all([
    AsyncStorage.removeItem('session'),
    AsyncStorage.removeItem('president'),
    AsyncStorage.removeItem('councilor'),
  ]);

const getStoreData = async () => {
  const [session, president, councilor] = await Promise.all([
    AsyncStorage.getItem('session'),
    AsyncStorage.getItem('president'),
    AsyncStorage.getItem('councilor'),
  ]);

  return {
    storedSession: session ? JSON.parse(session) : null,
    president,
    councilor,
  };
};
export { removeStoreData, getStoreData };
