import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';

const axiosApiInstance = axios.create();

axiosApiInstance.interceptors.request.use(
  async config => {
    const token = (await AsyncStorage.getItem('token')) as string;

    if (!token) {
      return Promise.reject({ message: 'Token is not valid' });
    }

    config.headers = {
      Authorization: token,
      Accept: 'application/json',
    };
    return config;
  },
  error => {
    Promise.reject(error);
  },
);

export default axiosApiInstance;
