import axios from 'axios';
import { API_URL } from '@env';
import { ILogin } from '@shared/models';

const authService = {
  login: ({ username, password }: ILogin) =>
    axios({ method: 'get', url: `${API_URL}/login?username=${username}&password=${password}` }),
  signup: ({ username, password }: ILogin) =>
    axios({ method: 'post', url: `${API_URL}/signup`, data: { username, password } }),
};

export { authService };
