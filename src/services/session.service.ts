import { API_URL } from '@env';
import axios from './instance';

const sessionService = {
  create: (amountOfPlayers: number) =>
    axios({ method: 'post', url: `${API_URL}/sessions`, data: { amountOfPlayers } }),
  join: (userId: string, sessionId: string) =>
    axios({ method: 'patch', url: `${API_URL}/sessions/${sessionId}?type=join`, data: { userId } }),
  leave: (userId: string, sessionId: string) =>
    axios({
      method: 'patch',
      url: `${API_URL}/sessions/${sessionId}?type=leave`,
      data: { userId },
    }),
  get: (sessionId: string) => axios({ method: 'get', url: `${API_URL}/sessions/${sessionId}` }),
  updatePlayer: ({
    userId,
    sessionId,
    type,
  }: {
    userId: string;
    sessionId: string;
    type: string;
  }) =>
    axios({
      method: 'patch',
      url: `${API_URL}/sessions/${sessionId}/users/${userId}`,
      data: { type },
    }),
};

export { sessionService };
