import { StyleSheet, Dimensions } from 'react-native';
import { Colors } from '@shared/colors';

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    width: '100%',
    justifyContent: 'center',
    flex: 1,
  },

  circleContainer: {
    alignContent: 'center',
    justifyContent: 'center',
    flex: 1,
  },

  headerContainer: {
    width: '100%',
    marginTop: 20,
    justifyContent: 'center',
  },

  headerTextContainer: {
    alignItems: 'center',
    marginBottom: 5,
  },

  circle: {
    borderRadius: Math.round(Dimensions.get('window').width + Dimensions.get('window').height) / 2,
    width: Dimensions.get('window').width * 0.8,
    height: Dimensions.get('window').width * 0.8,
    backgroundColor: Colors.main,
  },

  footer: {
    marginBottom: 10,
    width: '100%',
    justifyContent: 'space-between',
  },

  user0: {
    position: 'absolute',
    left: Dimensions.get('window').width * 0.3,
    top: Dimensions.get('window').width * 0.7,
  },

  user1: {
    position: 'absolute',
    top: Dimensions.get('window').width * 0.06,
  },

  user2: {
    position: 'absolute',
    top: Dimensions.get('window').width * 0.6,
    left: Dimensions.get('window').width * 0.02,
  },

  user3: {
    position: 'absolute',
    left: Dimensions.get('window').width * 0.62,
    top: Dimensions.get('window').width * 0.05,
  },

  user4: {
    position: 'absolute',
    left: Dimensions.get('window').width * 0.6,
    top: Dimensions.get('window').width * 0.6,
  },

  user5: {
    position: 'absolute',
    left: Dimensions.get('window').width * 0.3,
    top: Dimensions.get('window').width * -0.08,
  },

  user6: {
    position: 'absolute',
    left: Dimensions.get('window').width * 0.7,
    top: Dimensions.get('window').width * 0.33,
  },

  user7: {
    position: 'absolute',
    left: Dimensions.get('window').width * -0.09,
    top: Dimensions.get('window').width * 0.33,
  },

  president: {
    backgroundColor: Colors.component,
  },

  councilor: {
    backgroundColor: Colors.dark,
  },

  unvotedRoundsContainer: {
    backgroundColor: Colors.main,
    padding: 10,
    flexDirection: 'row',
  },

  unvotedRounds: {
    marginLeft: 10,
    color: Colors.component,
    fontSize: 15,
  },
});

export { styles };
