import React from 'react';
import { Dialog, Text } from '@rneui/themed';
import { useDispatch, useSelector } from 'react-redux';

import { IPlayer } from '@shared/models';
import { LoaderNames } from '@shared/enums';
import { selectIsLoading } from '@store/loaders';
import { Button } from '@components';
import { acceptRole } from '@store/sessions';
import { ActivityIndicator } from 'react-native';

const Role: React.FC<{ player: IPlayer }> = ({ player }) => {
  const dispatch = useDispatch();
  const isLoading = useSelector(selectIsLoading(LoaderNames.sessionsAcceptRole));

  return (
    <Dialog isVisible>
      <Text>Твоя Роль</Text>
      <Text>{player.role}</Text>
      {isLoading ? (
        <ActivityIndicator size="large" />
      ) : (
        <Button title="ОК" onPress={() => dispatch(acceptRole())} />
      )}
    </Dialog>
  );
};

export { Role };
