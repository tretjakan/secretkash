import React from 'react';
import { Dialog } from '@rneui/themed';
import { ActivityIndicator, View } from 'react-native';
import { useLinkTo } from '@react-navigation/native';
import { useDispatch, useSelector } from 'react-redux';

import { Button } from '@components';
import { leaveSession } from '@store/sessions';
import { selectIsLoading } from '@store/loaders';
import { LoaderNames } from '@shared/enums';

const Menu: React.FC<{ isOpen: boolean; onClose: () => void }> = ({ isOpen, onClose }) => {
  const dispatch = useDispatch();
  const linkTo = useLinkTo();
  const isLoading = useSelector(selectIsLoading(LoaderNames.sessionsLeave));

  const onExit = () => {
    dispatch(leaveSession.init(() => linkTo('/Home')));
    onClose();
  };

  return (
    <View>
      <Dialog isVisible={isOpen} onBackdropPress={onClose}>
        <Dialog.Title title="Меню" />
        {isLoading ? <ActivityIndicator size="large" /> : <Button title="Вийти" onPress={onExit} />}
      </Dialog>
    </View>
  );
};

export { Menu };
