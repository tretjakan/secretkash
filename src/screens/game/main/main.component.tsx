import React, { useState } from 'react';
import { TouchableHighlight, View } from 'react-native';
import { useSelector } from 'react-redux';
import { Text } from '@rneui/themed';
import { Avatar, Button } from '@components';
import { Colors } from '@shared/colors';
import {
  selectCurrentSession,
  selectPlayer,
  selectPresident,
  selectProposedCouncilor,
  selectCouncilor,
  selectRound,
  selectUnvotedRounds,
} from '@store/sessions';
import { selectUser } from '@store/auth';
import { VoteType } from '@shared/models';

import { Rules } from './rules';
import { Menu } from './menu';
import { UsersAction, VoteCouncilor } from './users-action';
import { Role } from './role';
import { Round } from './round';

import { styles } from './main.styles';

const MainGame = () => {
  const [voteType, setVoteType] = useState<VoteType | null>(null);
  const currentUser = useSelector(selectUser);
  const player = useSelector(selectPlayer);
  const session = useSelector(selectCurrentSession);
  const president = useSelector(selectPresident);
  const councilor = useSelector(selectCouncilor);
  const proposedCouncilor = useSelector(selectProposedCouncilor);
  const unvotedRounds = useSelector(selectUnvotedRounds);
  const round = useSelector(selectRound);
  const [isMenuOpen, setMenuOpen] = useState(false);

  return (
    <View style={styles.container}>
      <View style={styles.headerContainer}>
        {unvotedRounds > 0 && (
          <View style={styles.unvotedRoundsContainer}>
            <Text>Непроголосовані раунди:</Text>
            <Text style={styles.unvotedRounds}>{unvotedRounds}</Text>
          </View>
        )}
        <View style={styles.headerTextContainer}>
          <Text h4>Рахунок</Text>
        </View>
        <Rules />
      </View>
      <View style={styles.circleContainer}>
        <TouchableHighlight
          style={styles.circle}
          underlayColor="#ccc"
          onPress={() => console.log('Yaay!')}
        >
          <View>
            {session.users.map((user, index) => (
              <View
                key={user.id}
                style={[
                  (styles as { [key: string]: any })[`user${index}`],
                  president === user.id && styles.president,
                  councilor === user.id && styles.councilor,
                ]}
              >
                <Avatar label={user.username} />
              </View>
            ))}
          </View>
        </TouchableHighlight>
      </View>
      <View style={styles.footer}>
        {president === currentUser._id && (
          <Button
            title="Назначити Радника"
            color={Colors.primary}
            onPress={() => setVoteType('councilor')}
          />
        )}
        <Button title="Меню" color={Colors.secondary} onPress={() => setMenuOpen(true)} />
      </View>
      <Menu isOpen={isMenuOpen} onClose={() => setMenuOpen(false)} />
      {voteType && (
        <UsersAction
          type={voteType}
          session={session}
          user={currentUser}
          onClose={() => setVoteType(null)}
        />
      )}
      {player?.type === 'pending' && <Role player={player} />}
      {proposedCouncilor && <VoteCouncilor session={session} userId={currentUser._id} />}
      {round === 1 && currentUser._id === president && <Round round={round} />}
      {round === 2 && currentUser._id === councilor && <Round round={round} />}
    </View>
  );
};

export { MainGame };
