import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ActivityIndicator } from 'react-native';
import { Text, Dialog } from '@rneui/themed';

import {
  publishMessage,
  selectIfPlayerChoseProposedCouncilor,
  selectPresident,
  selectProposedCouncilor,
} from '@store/sessions';
import { ISession } from '@shared/models';

const VoteCouncilor: React.FC<{ session: ISession; userId: string }> = ({ session, userId }) => {
  const dispatch = useDispatch();
  const isUserMadeChoice = useSelector(selectIfPlayerChoseProposedCouncilor);
  const president = useSelector(selectPresident);
  const proposedCouncilor = useSelector(selectProposedCouncilor);

  const onVote = (answer: boolean) => {
    dispatch(
      publishMessage({ event: 'vote-councilor', data: { sessionId: session._id, userId, answer } }),
    );
  };

  const user1 = session.users.find(user => user.id === president);
  const user2 = session.users.find(user => user.id === proposedCouncilor);

  return (
    <Dialog>
      <Text>{`Чи ти згоден з вибором Президент: ${user1?.username} та Радник: ${user2?.username}?`}</Text>
      {isUserMadeChoice ? (
        <ActivityIndicator size="large" />
      ) : (
        <Dialog.Actions>
          <Dialog.Button title="Ні" onPress={() => onVote(false)} />
          <Dialog.Button title="Так" onPress={() => onVote(true)} />
        </Dialog.Actions>
      )}
    </Dialog>
  );
};

export { VoteCouncilor };
