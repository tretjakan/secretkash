import { Colors } from '@shared/colors';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  header: {
    margin: 10,
  },
  content: {
    backgroundColor: Colors.neutral,
  },
});

export { styles };
