import React from 'react';
import { TouchableHighlight } from 'react-native';
import { useDispatch } from 'react-redux';
import { ISession, IUser, VoteType } from '@shared/models';
import { Text, ListItem, Dialog } from '@rneui/themed';
import { publishMessage } from '@store/sessions';

import { styles } from './users-action.styles';

const UsersAction: React.FC<{
  type: VoteType;
  session: ISession;
  user: IUser;
  onClose: () => void;
}> = ({ type, session: { users, _id }, user: currentUser, onClose }) => {
  const dispatch = useDispatch();

  const onVote = (userId: string) => {
    if (type === 'councilor') {
      dispatch(publishMessage({ event: 'select-councilor', data: { sessionId: _id, userId } }));
    } else {
      console.log('TODO');
    }
    onClose();
  };

  return (
    <Dialog isVisible onBackdropPress={() => onClose()}>
      <Text style={styles.header}>{type === 'councilor' ? 'Обрати Радника' : 'Вигнати з Гри'}</Text>
      {users
        .filter(user => user.id !== currentUser._id)
        .map(user => (
          <ListItem
            key={user.id}
            containerStyle={styles.content}
            Component={TouchableHighlight}
            bottomDivider
            onPress={() => onVote(user.id)}
          >
            <ListItem.Content>
              <ListItem.Title>{user.username}</ListItem.Title>
            </ListItem.Content>
          </ListItem>
        ))}
    </Dialog>
  );
};

export { UsersAction };
