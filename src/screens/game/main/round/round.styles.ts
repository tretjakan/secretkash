import { StyleSheet } from 'react-native';
import { Colors } from '@shared/colors';

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    height: '30%',
  },

  rulContainer: {
    width: '100%',
    flexDirection: 'row',
    height: '70%',
    alignItems: 'center',
    marginTop: 10,
    justifyContent: 'center',
  },

  rule: {
    width: 75,
    height: '100%',
    backgroundColor: Colors.main,
    marginRight: 10,
  },
});

export { styles };
