import React from 'react';
import { View, Text, TouchableHighlight } from 'react-native';
import { Dialog } from '@rneui/themed';

import { styles } from './round.styles';

const Round: React.FC<{ round: number }> = ({ round }) => {
  const onPressRule = () => {
    console.log('rule pressed');
  };

  return (
    <Dialog isVisible overlayStyle={styles.container}>
      <Dialog.Title title={`Раунд ${round}`} />
      <Text>Обери правило</Text>
      <View style={styles.rulContainer}>
        <TouchableHighlight style={styles.rule} underlayColor="#ccc" onPress={() => onPressRule()}>
          <View />
        </TouchableHighlight>
        <TouchableHighlight style={styles.rule} underlayColor="#ccc" onPress={() => onPressRule()}>
          <View />
        </TouchableHighlight>
        {round === 1 && (
          <TouchableHighlight
            style={styles.rule}
            underlayColor="#ccc"
            onPress={() => onPressRule()}
          >
            <View />
          </TouchableHighlight>
        )}
      </View>
    </Dialog>
  );
};

export { Round };
