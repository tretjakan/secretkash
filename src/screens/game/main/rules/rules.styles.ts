import { Colors } from '@shared/colors';
import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    width: '100%',
    alignItems: 'center',
  },

  rulesContainer: {
    borderRadius: 5,
    width: '95%',
    height: 50,
    marginLeft: 5,
  },

  rulesWrapper: {
    width: '60%',
    flexDirection: 'row',
    alignItems: 'center',
  },

  rulesItems: {
    flexDirection: 'row',
  },

  liberal: {
    backgroundColor: Colors.component,
    marginBottom: 5,
  },

  nazi: {
    backgroundColor: Colors.component,
  },

  ruleContainer: {
    height: 40,
    width: 30,
    margin: 5,
    borderRadius: 5,
  },

  liberalRule: {
    backgroundColor: Colors.primary,
  },

  naziRule: {
    backgroundColor: Colors.main,
  },
});

export { styles };
