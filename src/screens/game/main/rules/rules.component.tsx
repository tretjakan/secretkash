import React, { useState } from 'react';
import { Icon } from '@rneui/themed';
import { SafeAreaView, View, TouchableHighlight } from 'react-native';
import { RuleTypes } from '@shared/enums';

import { RuleDescription } from './rule-description.component';
import { styles } from './rules.styles';

const Rule: React.FC<{ type: RuleTypes }> = ({ type }) => {
  return (
    <SafeAreaView
      style={[styles.ruleContainer, type === 'liberal' ? styles.liberalRule : styles.naziRule]}
    />
  );
};

const Rules = () => {
  const [type, setType] = useState<RuleTypes | null>(null);

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.rulesWrapper}>
        <Icon name="account-balance" />
        <TouchableHighlight
          style={[styles.rulesContainer, styles.liberal]}
          onPress={() => setType(RuleTypes.liberal)}
        >
          <View style={styles.rulesItems}>
            <Rule type={RuleTypes.liberal} />
            <Rule type={RuleTypes.liberal} />
          </View>
        </TouchableHighlight>
      </View>

      <View style={styles.rulesWrapper}>
        <Icon name="face" />
        <TouchableHighlight
          style={[styles.rulesContainer, styles.nazi]}
          onPress={() => setType(RuleTypes.nazi)}
        >
          <View style={styles.rulesItems}>
            <Rule type={RuleTypes.nazi} />
          </View>
        </TouchableHighlight>
      </View>
      <RuleDescription isOpen={Boolean(type)} onClose={() => setType(null)} type={type} />
    </SafeAreaView>
  );
};

export { Rules };
