import React from 'react';
import { Dialog } from '@rneui/themed';

import { IModal } from '@shared/models';
import { RuleTypes } from '@shared/enums';

const RuleDescription: React.FC<IModal & { type: RuleTypes | null }> = ({
  isOpen,
  onClose,
  type,
}) => {
  return (
    <Dialog isVisible={isOpen} onBackdropPress={onClose}>
      <Dialog.Title
        title={
          type === RuleTypes.liberal ? 'Прийнятті ліберальні правила' : 'Прийняті наці правила'
        }
      />
    </Dialog>
  );
};

export { RuleDescription };
