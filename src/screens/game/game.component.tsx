import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { SafeAreaView } from 'react-native';
import { connectSocket, disconnectSocket, selectCurrentSession } from '@store/sessions';

import { styles } from './game.styles';
import { MainGame } from './main';
import { PendingGame } from './pending';

const GameScreen = () => {
  const session = useSelector(selectCurrentSession);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(connectSocket());

    return () => {
      console.log('did mount');
      dispatch(disconnectSocket());
    };
  }, [dispatch]);

  return (
    <SafeAreaView style={styles.container}>
      {session?.status === 'active' ? <MainGame /> : <PendingGame />}
    </SafeAreaView>
  );
};

export { GameScreen };
