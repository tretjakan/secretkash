import { StyleSheet } from 'react-native';
import { Colors } from '@shared/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.palette,
    alignItems: 'center',
  },
});

export { styles };
