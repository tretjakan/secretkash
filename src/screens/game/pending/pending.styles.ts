import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  progress: {
    marginVertical: 10,
  },

  cancel: {
    marginTop: 20,
  },

  nameContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  nameHeader: {
    fontSize: 18,
    marginRight: 5,
  },
});

export { styles };
