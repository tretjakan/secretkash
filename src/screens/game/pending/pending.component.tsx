import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useLinkTo } from '@react-navigation/native';
import { ActivityIndicator, View } from 'react-native';
import Clipboard from '@react-native-community/clipboard';
import { Dialog, Text, LinearProgress, FAB } from '@rneui/themed';
import { Colors } from '@shared/colors';
import { leaveSession, selectCurrentSession } from '@store/sessions';
import { selectIsLoading } from '@store/loaders';
import { LoaderNames } from '@shared/enums';
import { Button } from '@components';

import { styles } from './pending.styles';

const PendingGame = () => {
  const session = useSelector(selectCurrentSession);
  const isLoading = useSelector(selectIsLoading(LoaderNames.sessionsLeave));
  const dispatch = useDispatch();
  const linkTo = useLinkTo();

  const onCancel = () => {
    dispatch(leaveSession.init(() => linkTo('/Home')));
  };

  const onCopyToClipboard = () => {
    Clipboard.setString(session.name);
  };

  return (
    <Dialog isVisible>
      <Dialog.Title title="Чекаємо Гравців" />
      <Text>{`${session.users?.length} з ${session?.configuration?.amountOfPlayers} підключились`}</Text>
      <LinearProgress style={styles.progress} color={Colors.dark} />
      <View style={styles.nameContainer}>
        <Text style={styles.nameHeader}>{`ІД Гри: ${session.name}`}</Text>
        <FAB
          visible
          icon={{ name: 'content-copy', color: 'white' }}
          onPress={onCopyToClipboard}
          size="small"
        />
      </View>
      {isLoading ? (
        <ActivityIndicator size="large" />
      ) : (
        <Button title="Скасувати" color={Colors.main} style={styles.cancel} onPress={onCancel} />
      )}
    </Dialog>
  );
};

export { PendingGame };
