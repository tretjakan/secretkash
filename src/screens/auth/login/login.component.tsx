import React from 'react';
import { useDispatch } from 'react-redux';
import { SafeAreaView } from 'react-native-safe-area-context';
import { View, TextInput } from 'react-native';
import { useForm, Controller } from 'react-hook-form';
import { Button } from '@rneui/themed';
import { Colors } from '@shared/colors';
import { Logo } from '@components';
import { login } from '@store/auth';

import { styles } from './login.styles';

const LoginScreen = () => {
  const dispatch = useDispatch();
  const { control, handleSubmit } = useForm({
    defaultValues: {
      username: '',
      password: '',
    },
  });
  const onSubmit = (data: any) => {
    console.log(data);
    dispatch(login.init(data));
  };

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.header}>
        <Logo />
      </View>
      <View style={styles.inputView}>
        <Controller
          control={control}
          rules={{
            required: true,
          }}
          render={({ field: { onChange, onBlur, value } }) => (
            <TextInput
              autoFocus
              style={styles.textInput}
              autoCapitalize="none"
              onBlur={onBlur}
              onChangeText={onChange}
              value={value}
              placeholder="Username"
              placeholderTextColor={Colors.dark}
            />
          )}
          name="username"
        />
      </View>

      <View style={styles.inputView}>
        <Controller
          control={control}
          rules={{
            required: true,
          }}
          render={({ field: { onChange, onBlur, value } }) => (
            <TextInput
              style={styles.textInput}
              onBlur={onBlur}
              onChangeText={onChange}
              value={value}
              placeholder="Password"
              placeholderTextColor={Colors.dark}
              secureTextEntry
            />
          )}
          name="password"
        />
      </View>

      <View style={styles.actions}>
        <Button
          color={Colors.secondary}
          title="Login"
          onPress={handleSubmit(onSubmit)}
          buttonStyle={styles.buttons}
        />
        <Button
          title="Register"
          color={Colors.dark}
          onPress={() => console.log('register')}
          buttonStyle={styles.buttons}
        />
      </View>
    </SafeAreaView>
  );
};

export { LoginScreen };
