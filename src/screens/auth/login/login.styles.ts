import { StyleSheet } from 'react-native';
import { Colors } from '@shared/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.main,
    alignItems: 'center',
    justifyContent: 'center',
  },

  header: {
    marginBottom: 40,
  },

  logo: {
    width: 150,
    height: 100,
  },

  headerText: {
    color: Colors.component,
    fontSize: 40,
  },

  inputView: {
    backgroundColor: Colors.component,
    borderRadius: 5,
    width: '80%',
    height: 65,
    marginBottom: 10,
  },

  textInput: {
    height: 70,
    flex: 1,
    padding: 10,
    marginLeft: 20,
    color: Colors.dark,
  },

  actions: {
    marginTop: 15,
    width: '80%',
  },

  buttons: {
    marginTop: 10,
    borderRadius: 5,
  },
});

export { styles };
