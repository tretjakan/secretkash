import { StyleSheet } from 'react-native';
import { Colors } from '@shared/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.palette,
    alignItems: 'center',
    justifyContent: 'center',
  },

  wrapper: {
    flex: 7,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },

  logoContainer: {
    flex: 2,
  },

  actionsContainer: {
    width: '80%',
  },

  action: {
    borderRadius: 5,
    marginBottom: 10,
  },
});

export { styles };
