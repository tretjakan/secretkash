import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { View } from 'react-native';
import { useLinkTo } from '@react-navigation/native';
import { Colors } from '@shared/colors';
import { Logo, Button } from '@components';
import { logout } from '@store/auth';
import { checkSession, selectCurrentSession } from '@store/sessions';

import { JoinGame } from './join-game';
import { CreateGame } from './create-game';

import { styles } from './home.styles';

const HomeScreen = () => {
  const dispatch = useDispatch();
  const session = useSelector(selectCurrentSession);
  const linkTo = useLinkTo();
  const [isOpenJoinGame, setOpenJoinGame] = useState(false);
  const [isOpenCreateGame, setOpenCreateGame] = useState(false);

  useEffect(() => {
    dispatch(checkSession());
  }, [dispatch]);

  useEffect(() => {
    switch (session.status) {
      case 'pending':
      case 'active': {
        console.log('to game screen');
        linkTo('/Game');
        break;
      }
      case 'closed': {
        console.log('to home screen');
        linkTo('/Home');
        break;
      }
    }
  }, [session?.status, linkTo]);

  return (
    <View style={styles.container}>
      <View style={styles.wrapper}>
        <View style={styles.actionsContainer}>
          <Button
            title="Створити гру"
            color={Colors.dark}
            onPress={() => setOpenCreateGame(true)}
          />
          <Button
            title="Приєднатися до гри"
            color={Colors.secondary}
            onPress={() => setOpenJoinGame(true)}
          />
        </View>
      </View>
      <View style={styles.logoContainer}>
        <Button
          title="Вихід"
          color={Colors.main}
          onPress={() => dispatch(logout())}
          buttonStyle={styles.action}
        />
        <Logo />
      </View>
      {isOpenJoinGame && (
        <JoinGame isOpen={isOpenJoinGame} onClose={() => setOpenJoinGame(false)} />
      )}
      {isOpenCreateGame && (
        <CreateGame isOpen={isOpenCreateGame} onClose={() => setOpenCreateGame(false)} />
      )}
    </View>
  );
};

export { HomeScreen };
