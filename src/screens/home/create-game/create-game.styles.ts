import { StyleSheet } from 'react-native';
import { Colors } from '@shared/colors';

const styles = StyleSheet.create({
  textInput: {
    padding: 10,
    color: Colors.dark,
  },
  inputView: {
    backgroundColor: Colors.component,
    borderRadius: 5,
  },
  inputHeader: {
    marginLeft: 10,
    marginBottom: 5,
  },
});

export { styles };
