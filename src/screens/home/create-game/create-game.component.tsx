import React from 'react';
import { View, TextInput, ActivityIndicator } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { useForm, Controller } from 'react-hook-form';
import { Dialog, Text } from '@rneui/themed';
import { ICreateGame } from '@shared/models';

import { createSession } from '@store/sessions';
import { selectIsLoading } from '@store/loaders';
import { LoaderNames } from '@shared/enums';

import { styles } from './create-game.styles';

const CreateGame: React.FC<ICreateGame> = ({ isOpen, onClose }) => {
  const isLoading = useSelector(selectIsLoading(LoaderNames.sessionsCreate));
  const dispatch = useDispatch();
  const { control, handleSubmit } = useForm({
    defaultValues: {
      amountOfPlayers: '5',
    },
  });

  const onSubmit = (data: any) => {
    console.log(data);
    dispatch(
      createSession.init({
        amountOfPlayers: data.amountOfPlayers,
        callback: () => {
          onClose();
        },
      }),
    );
  };

  return (
    <View>
      <Dialog isVisible={isOpen} onBackdropPress={onClose}>
        <Dialog.Title title="Створити Гру" />
        <Text style={styles.inputHeader}>Кількість гравців</Text>
        {isLoading ? (
          <ActivityIndicator size="large" />
        ) : (
          <View style={styles.inputView}>
            <Controller
              control={control}
              rules={{
                required: true,
              }}
              render={({ field: { onChange, onBlur, value } }) => (
                <TextInput
                  autoFocus
                  style={styles.textInput}
                  onBlur={onBlur}
                  onChangeText={onChange}
                  value={value}
                  keyboardType="numeric"
                />
              )}
              name="amountOfPlayers"
            />
          </View>
        )}
        <Dialog.Actions>
          <Dialog.Button title="ОК" onPress={handleSubmit(onSubmit)} />
        </Dialog.Actions>
      </Dialog>
    </View>
  );
};

export { CreateGame };
