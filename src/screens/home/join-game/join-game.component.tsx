import React from 'react';
import { View, TextInput, ActivityIndicator } from 'react-native';
import { useForm, Controller } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { Dialog, Text } from '@rneui/themed';
import { IJoinGame } from '@shared/models';
import { selectIsLoading } from '@store/loaders';
import { LoaderNames } from '@shared/enums';

import { styles } from './join-game.styles';
import { joinSession } from '@store/sessions';

const JoinGame: React.FC<IJoinGame> = ({ isOpen, onClose }) => {
  const isLoading = useSelector(selectIsLoading(LoaderNames.sessionsJoin));
  const dispatch = useDispatch();

  const { control, handleSubmit } = useForm({
    defaultValues: {
      name: '',
    },
  });

  const onSubmit = (data: any) => {
    console.log(data);
    dispatch(joinSession.init({ callback: () => onClose(), name: data.name }));
  };

  return (
    <View>
      <Dialog isVisible={isOpen} onBackdropPress={onClose}>
        <Dialog.Title title="Приєднатись До Гри" />
        <Text style={styles.inputHeader}>ІД Гри</Text>
        {isLoading ? (
          <ActivityIndicator size="large" />
        ) : (
          <View style={styles.inputView}>
            <Controller
              control={control}
              rules={{
                required: true,
              }}
              render={({ field: { onChange, onBlur, value } }) => (
                <TextInput
                  autoFocus
                  style={styles.textInput}
                  onBlur={onBlur}
                  onChangeText={onChange}
                  autoCapitalize="none"
                  value={value}
                />
              )}
              name="name"
            />
          </View>
        )}
        <Dialog.Actions>
          <Dialog.Button title="ОК" onPress={handleSubmit(onSubmit)} />
        </Dialog.Actions>
      </Dialog>
    </View>
  );
};

export { JoinGame };
